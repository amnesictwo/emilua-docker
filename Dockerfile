FROM archlinux

# compilation stuff
RUN pacman -Syuq --noconfirm
RUN pacman -Syq --noconfirm sudo nano fakeroot git
RUN pacman -Syq --noconfirm meson ninja base-devel
RUN pacman -Syq --noconfirm re2c xxd asciidoctor fmt
RUN pacman -Syq --noconfirm binutils boost luajit liburing

# emilua-doc for asciidoc stuff
RUN if [[ $NO_ENABLE_DOC != "true" ]]; then \
    pacman -Syq --noconfirm pkgconfig pandoc asciidoctor ruby; \
    fi

# console utility
RUN pacman -Syq --noconfirm screen


# stage for build
ENV BUILDER_NAME=builder
RUN useradd -m $BUILDER_NAME
ENV HOME_BUILDER=/home/$BUILDER_NAME

# grouping stuff
ENV EMILUA_GROUPS=emilua_resourcer

RUN groupadd $EMILUA_GROUPS
RUN usermod -a -G $EMILUA_GROUPS $BUILDER_NAME

USER $BUILDER_NAME

WORKDIR $HOME_BUILDER

# build package
ENV PKGBUILD_URL_EMILUA=https://gitlab.com/amnesictwo/emilua/-/raw/master/PKGBUILD

RUN curl -Oq $PKGBUILD_URL_EMILUA

RUN makepkg

# install package
RUN curl -q $PKGBUILD_URL_EMILUA | awk -F = '$1 == "pkgver" { print $2 }' > pkg_version

RUN curl -q $PKGBUILD_URL_EMILUA | awk -F = '$1 == "pkgrel" { print $2 }' > pkg_release

USER root

WORKDIR $HOME_BUILDER

RUN pacman -U --noconfirm $HOME_BUILDER/emilua-$(cat pkg_version)-$(cat pkg_release)-$(uname -m).pkg.tar.zst

WORKDIR /root

# user for
ENV EMILUA_USER_CONSOLE=emilua_vm
RUN useradd -m $EMILUA_USER_CONSOLE
ENV HOME_EMILUA=/home/$EMILUA_USER_CONSOLE

RUN usermod -a -G $EMILUA_GROUPS $EMILUA_USER_CONSOLE

 
RUN echo "$EMILUA_USER_CONSOLE:$EMILUA_USER_CONSOLE" | chpasswd
 
RUN echo "$EMILUA_USER_CONSOLE ALL=(ALL:ALL) ALL" >> /etc/sudoers

USER $EMILUA_USER_CONSOLE

WORKDIR $HOME_EMILUA


